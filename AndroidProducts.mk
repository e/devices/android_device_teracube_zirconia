#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_zirconia.mk

COMMON_LUNCH_CHOICES := \
    lineage_zirconia-user \
    lineage_zirconia-userdebug \
    lineage_zirconia-eng